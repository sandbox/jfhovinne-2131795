<?php

/**
 * @file
 * drush_send_email - Drush command to send e-mail messages.
 *
 * Copyright (C) 2012 by the original authors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * Implementation of hook_drush_command().
 */
function drush_send_email_drush_command() {
  $items = array();

  $items['send-email'] = array(
    'description' => "Sends an e-mail message to the specified e-mail address using standard Drupal mail system.",
    'arguments' => array(
      'to' => 'The e-mail address to send the message to.',
      'subject' => 'Optional. The message subject. Default is "Test message"',
      'body' => 'Optional. The message body. Default is "This is a test."',
    ),
    'required-arguments' => 1,
    'options' => array(
      'from' => array(
        'description' => 'The message sender e-mail address.',
      ),
    ),
    'examples' => array(
      'drush email foo@example.com' => 'Sends a message to foo@example.com.',
      'drush email admin "Hello" "Hello, World"' => 'Sends a message to admin user with the specified subject and body.',
      'drush email admin --from=bar@example.com' => 'Sends a message from bar@example.com to admin.',
    ),
    'aliases' => array('email'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  return $items;
}

/**
 * Send an e-mail to a specified e-mail address.
 */
function drush_drush_send_email_send_email($to, $subject = 'Test message', $body = 'This is a test.') {
  // Define $from and headers
  if (!$from = drush_get_option('from')) $from = variable_get('site_mail', ini_get('sendmail_from'));
  $headers = array();
  $headers['From'] = $headers['Sender'] = $headers['Return-Path'] = $headers['Errors-To'] = $from;

  // If $to isn't a valid e-mail address, try to load the account based on the username.
  if(!valid_email_address($to)) {
    if(function_exists('user_load_by_name')) $account = user_load_by_name($to);
    else $account = user_load(array('name' => $to));
    if(empty($account)) return drush_set_error('DRUSH_BAD_EMAIL_OR_USERNAME', dt('Bad e-mail address or username.'));
    else $to = $account->mail;
  }

  // D7 implementation of drupal_mail
  if(function_exists('drupal_mail_system')) {
    // Prepare the message.
    $message = drupal_mail('drush', 'key', $to, NULL, array(), $from, FALSE);

    $message['subject'] = $subject;
    $message['body'] = array();
    $message['body'][] = $body;
    $message['headers'] = $headers;

    // Retrieve the responsible implementation for this message.
    $system = drupal_mail_system('drush', 'key');
    // Format the message body.
    $message = $system->format($message);
    // Send e-mail.
    $message['result'] = $system->mail($message);
    $result = $message['result'];

  // D6 implementation of drupal_mail_send
  } else {
    $message = array(
      'to' => $to,
      'subject' => $subject,
      'body' => $body,
      'headers' => $headers,
    );
    $result = drupal_mail_send($message);
  }

  // Return result.
  if($result) drush_log(dt('E-mail message sent to <!to>', array('!to' => $to)), 'ok');
  else drush_set_error('DRUSH_MAIL_ERROR', dt('An error occurred while sending the e-mail message.'));
}

